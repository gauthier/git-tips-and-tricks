# Trucs et astuces Git
## Aperçu

Cette page donne quelques trucs et astuces utiles sur l'utilisation de git.

## Quelques détails pertinents de Git

On suppose à ce stade que vous avez une petite connaissance de CVS et/ou de Subversion; et que vous avez au moins obtenu une copie locale (clonée) d'un dépôt distant.
- Contrairement à CVS et SVN, chaque zone de travail locale est également un référentiel (dépôt).
- Contrairement à CVS et SVN, les dépôts ne sont pas divisibles: dans les systèmes précédents, on peut facilement vérifier seulement une sous-section d'un paquet; Avec git, c'est tout ou rien.
- Un référentiel peut avoir zéro, un ou plusieurs dépôts distant vers lesquels les modifications peuvent être poussées (ou à partir desquels ils peuvent être récupérés): Git est un système véritablement distribué.
- Les branches sont faciles à créer, à fusionner et à détruire.
- L'«unité de changement» est un commit, marqué par son hash SHA1. Un «état» d'un arbre est une collection de commit. La fusion des branches à plusieurs reprises est donc banale car elle consiste à comparer les listes de commit.
- Les arbres de commit Git n'enregistrent pas l'historique: ils enregistrent les modifications. Un changement simple a un parent. Une fusion non trivial de deux branches donne un commit à deux parents.

## Les bases de Git

### Obtenir de l'aide

- man git
- git help <command>
- man git-<command>
- [Le livre de la communauté git](https://git-scm.com/book/fr/v2)
- [Pragmatique Guide to Git](http://www.pragprog.com/titles/pg_git/pragmatic-guide-to-git)
- [Google](https://www.google.fr/search?q=git)

### Apprendre à manipuler les branches

- [Learn Git Branching](https://learngitbranching.js.org/)
- [Visualizing Git Concepts with D3](https://onlywei.github.io/explain-git-with-d3/)

### Comprendre les différents états des fichiers dans Git

- [GIT CHEATSHEET](http://ndpsoftware.com/git-cheatsheet.html#loc=workspace;)

### Glossaire

Les termes du glossaire ci-dessous sont volontairement laissés en anglais.

- **Add** : mettre un fichier (ou des modifications spécifiques) dans l'index prêt pour une opération de commit. Facultatif pour les modifications apportées aux fichiers suivis; obligatoire pour les fichiers jusqu'alors non renseignés.
- **Alias** raccourci pour une commande git (ou externe), stockée dans le fichier .gitconfig.
- **Branch** : un arbre de changement divergent, qui peut être fusionné en gros ou en partie avec la branche master.
- **Commit** : enregistre l'état actuel de l'index et/ou d'autres fichiers spécifiés dans le référentiel local.
- **Commit object** : un objet qui contient les informations sur une révision particulière, telles que parents, commit, auteur, date et l'objet arborescent qui correspond au répertoire supérieur de la révision stockée.
- **Dirty** : une zone de travail contenant des modifications non validées.
- **Fast-forward** : une opération de mise à jour consistant uniquement en l'application d'une partie linéaire de l'arbre de modification.
- **Fetch** : mettez à jour votre base de données de référentiel local ( pas votre zone de travail) avec les derniers changements à partir d'un dépôt distant.
- **HEAD** : le dernier état de la branche actuelle.
- **Index** : une collection de fichiers contenant des informations statistiques, dont les contenus sont stockés sous forme d'objets. L'index est une version stockée de votre arbre actif. Les fichiers doivent être ajoutés dans l'index avant d'être commité.
- **Master** : la branche principale: connue sous le nom de *trunc* dans d'autres systèmes de suivie de version.
- **Merge** : Fusion de deux arbres. Un commit est effectué si ce n'est pas une opération rapide (ou s'il est demandé explicitement).
- **Object** : l'unité de stockage en git. Il est identifié de manière unique par le hash SHA1 de son contenu. En conséquence, un objet ne peut pas être modifié.
- **Origin** : le dépôt distant par défaut, généralement la source de l'opération de clone qui a créé le référentiel local.
- **Plumbing** : Plomberie en fançais, fonctionnement interne, bas niveau de Git.
- **Porcelain** : niveau supérieur, interfaces visibles par l'utilisateur à la plomberie.
- **Pull** : raccourci pour une extraction suivie d'une fusion (ou rebase si l' --rebaseoption est utilisée).
- **Push** : transfère l'état de la branche actuelle à une branche de suivi à distance. Cela doit être une opération d'avance rapide (voir fusion).
- **Rebase** : une opération de fusion dans laquelle l'arbre de modification est réécrit (voir **Rebasing** ci-dessous). Utilisé pour transformer les fusions non triviales en opérations rapides *Fast-forward*.
- **Ref** : une représentation hexadécimal de 40 octets d'un SHA1 ou un nom qui désigne un objet particulier.
- **Remote** : un autre référentiel connu du référentiel local. Si le référentiel local a été créé avec "clone", il existe au moins un dépôt distant, habituellement appelé, "origin".
- **Stage** : pour ajouter un fichier ou des modifications sélectionnées de celui-ci à l'index en prévision d'un commit.
- **Stash** : une pile sur laquelle l'ensemble actuel de modifications non validées peut être mis (Ex. Pour passer ou synchroniser avec une autre branche) en tant que correctif pour la récupération plus tard.
- **Tag** : étiquette lisible par l'homme pour un état particulier de l'arbre. Les tags peuvent être simples (dans ce cas, ils sont en fait des branches) ou annotés (analogues à une balise CVS), avec un hash SHA1 associé et un message. Les étiquettes annotées sont préférables en général.
- **Tracking Branch** : une branche sur un dépôt distant qui est la source par défaut pour les opérations de push/fetch pour la branche en cours. Par exemple, origin/master est la branche de suivi du master local dans un référentiel local.
- **Tree-ish** : une référence indiquant soit un objet commit, un objet branch, soit un objet tag pointant sur une étiquette ou un commit ou un objet branch.
- **Un-tracked** : fichier inconnu pour git actuellement.

### Initialisation d'un référentiel

- Clone depuis un dépôt distant
```
git clone <repository-spec> <local-dir>
```
- ou création d'un nouveau dépôt local
```
mkdir <local-dir>
cd <local-dir>
git init
```

### Informations de base du journal.

```
git log []
```
- Astuce importante : les messages de commit ont une structure facultative, car de nombreuses commandes git ne regardent que la première ligne de commentaire du commit. Prenez l'habitude de ne présenter qu'un bref synopsis sur la première ligne d'un message de journal et de mettre des informations plus détaillées sur les lignes suivantes. Il est préférable de ne pas utiliser l'option **-m** et un éditeur de texte sera démarré pour saisir le commentaire.
- Cette commande est extrêmement polyvalente. Vous voudrez peut-être avoir quelques alias définis dans votre fichier .gitconfig (voir l'exemple de fichier [gitconfig](https://gist.github.com/pksunkara/988716) pour des idées).

- Exemples:
```
git log -p -2
git log --stat
git log --pretty=oneline
git log --pretty=format:"%h - %an, %ar : %s"
git log --pretty=format:"%h %s" --graph
git log --since=2.weeks
```

### Travailler dans votre dépôt local

- Obtenir la liste des fichiers modifiés avec
```
git status
```
- Déplacer les fichiers d'une partie de votre arborescence de répertoires vers un autre:
```
git mv <old-path> <new-path>
```
- Suppression des fichiers indésirables:
```
git rm <path>
```
- Suppression des fichiers de l'index (sans les supprimer du dossier):
```
git rm --cached <path>
```
- Ajouter des fichiers non renseignés:
```
git add <un-tracked-file>
```
- Ajout d'un fichier modifié pour le prochain commit:
```
git add <file>
```
- Commiter les fichiers actuellement ajoutés:
```
git commit -m <log-message>
```
- Commiter automatiquement tous les fichiers modifiés (sans ***git add***):
```
git commit -a -m <log-message>
```
- Détacher un fichier précédemment mis dans l'index (mais pas encore commité):
```
git reset HEAD <file>
```
- Examiner une représentation de votre arbre de modification avec les fichiers journaux et les descriptions de patch:
```
gitk
```
- Obtenir les différences par rapport à la version du dernier commit d'un fichier:
```
git diff <file>
```
- Obtenir les différences entre le fichier local et la version engagée:
```
git diff --cached <file>
```

### Interaction de base avec les branches locales

- Créer (mais ne pas passer à) une nouvelle branche locale basée sur la branche actuelle:
```
git branch <new-branch>
```
- Créer et passer à une branche locale en fonction de la branche actuelle:
```
git checkout -b <new-branch>
```
- Passer à une branche locale existante:
```
git checkout <branch>
```
- Examiner la liste des commits dans la branche en cours non présent dans une autre branche:
```
git cherry -v <branch>
```
- Fusionner une autre branche dans la branche actuelle:
```
git merge <branch>
```
- Supprimer une branche locale (Ex. Après la fusion):
```
git branch -d <branch>
```
OU (si les modifications n'ont pas été totalement fusionnées mais que vous êtes sûr de vouloir la supprimer):
```
git branch -D <branch>
```

### Interaction de base avec une branche distante

En supposant que vous ayez créé votre référentiel local avec *git clone*, il existe déjà un *origin* distant configuré et vous disposerez d'une branche locale pour chaque branche distante qui existait au moment de votre dernier *pull* ou *clone*.

- Obtenir la liste actuelle des dépôts distant (y compris les URI) avec:
```
git remote -v
```
- Obtenir la liste actuelle des branches créées avec:
```
git branch -a
```
- Modifier (créer si nécessaire) une branche locale qui suit une branche distante existante du même nom:
```
git checkout <branch>
```
- Mettre à jour votre référentiel local sans modifier la zone de travail actuelle:
```
git fetch <remote>
```
- Mettre à jour votre branche locale actuelle par rapport à l'idée actuelle de votre dépôt concernant l'état d'une branche à distance:
```
git merge <branch>
```
- Recupérer les modifications de tous les dépôts distants et fusionner les branches locales avec leurs branches de suivi à distance (le cas échéant):
```
git pull
```
- Examiner les modifications apportées à la branche locale actuelle par rapport à sa branche de suivi:
```
git cherry -v
```
- Poussez vos modifications sur le dépôt distant:
```
git push
```
- Poussez toutes les modifications de toutes les branches:
```
git push --all
```

## Des opérations plus avancées

- Astuce importante : si vous allez faire une opération dont le résultat est incertain, faites une copie de votre dépôt :
```
mkdir -p <path-to-safe-dir>
tar -cf - . | tar -xC <path-to-safe-dir>
```
L'espace disque est bon marché et un *rm -rf *est vite arrivé. Notez que vous devez copier tout le dépôt, car toutes les informations importantes se trouvent dans le repertoire *.git*.

### Stashing

C'est un bon moyen d'obtenir rapidement un arbre propre si vous voulez *merger* ou *rebaser* (voir ci-dessous) importer des modifications d'une branche sans avoir à engager votre travail actuel.
- Enregistrer les modifications non validées dans la zone de travail actuelle sur le stockage ( pas une opération de commit):
```
git stash
```
- Appliquer le cache existant précédemment:
```
git stash pop
```
(affiche les modifications et les applique à la zone de travail actuelle) ou
```
git stash apply
```
qui applique les modifications mais les conserve sur la pile.
- Examinez l'état actuel de la stash:
```
git stash list
```
- Effacer la totalité du stash:
```
git stash clear
```

### Rebasing

Rebase est un bon moyen pour:
1. Simplifier les branches et commits avant de pousser les modifications dans un dépôt distant.
2. Intégrer au fil de l'eau les modifications d'une branche d'un dépôt distant.

Vous pouvez rebaser des modifications locales que vous avez effectuées mais qui n’ont pas encore été partagées avant de les pousser de manière à obtenir un historique propre mais sans jamais rebaser quoi que ce soit que vous ayez déjà poussé quelque part.

- **Important** : **Ne rebasez jamais des commits qui ont déjà été poussés sur un dépôt public**.

#### Commit liés à squash:

Éclatez quelques-uns des derniers commit dans votre branche actuelle:
```
git rebase -i HEAD~5
```
Exécuter cette commande vous donne la liste des commits dans votre éditeur de texte (le plus récent en bas) ainsi que des instructions sur ce qu'il est possible de faire.
Les messages de commits peuvent être reformulés; les commits peuvent être supprimés, combinés avec d'autres commits ou déplacé.
Cette commande exécute une sorte de script qui s'arretera sur chaque point de modification en vous invitant à modifier le commit et à continuer.
```
Stopped at 7482e0d... updated the gemspec to hopefully work better
You can amend the commit now, with

       git commit --amend

Once you’re satisfied with your changes, run

       git rebase --continue
```

#### Ré-ordonner ou reformuler depuis la divergense d'une branche :

```
git rebase -i <branch>
```

#### Restez à jour avec les branches distantes sans *merger*.

```
git pull --rebase
```
ou
```
git fetch <remote>
git rebase <remote>/<branch>
```

### Résoudre les conflits

Toutes opérations *pull*, *merge* ou *rebase* peut entraîner un conflit lors de l'application d'un changement particulier de la branche à distance.
Suivez les instructions à l'écran pour résoudre les problèmes.
Cela consistera généralement à faire un **git status** pour obtenir la liste des conflits, à modifier les fichiers et à utiliser **git add** pour marquer chaque conflit résolu.
Le processus doit être soit autorisé à poursuivre en émettant un **git rebase --continue** ou **git merge --continue**, ou l'opération peut être inversée avec un **--abort** à la place de **--continue**. En cas de doute, copiez votre dépôt.

#### Rerere (Reuse recorded resolution)

- Git est capable d'enregistrer et de réutiliser les résolutions de conflits antérieures.
```
git config --global rerere.enabled 1
```

#### Ouvrir tous les fichiers en conflit dans un éditeur.

```
git diff --name-only | uniq | xargs $EDITOR
```

### Création d'une nouvelle branche distante

- Créez une nouvelle branche locale basée sur une version existante:
```
git checkout -b <branch>
```
- Faire des choses.
- Poussez la branche vers le repo distant :
```
git push <remote> <local-branch-name>[:<new-remote-branch-name>]
```

### Étiquetage

- Taguez l'état actuel d'une branche (p. Ex. Pour la sortie d'une nouvelle version):
```
git tag -am <message> <version>.
```
Notez que l'option **-a** crée une étiquette annoté, qui est elle-même un commit avec un hash et un message de commit.
C'est l'analogique le plus proche de la commande CVS tag. L'omission de l'option **-a** créera une "balise simple" qui est un pointeur vers un commit.
- Poussez vos modifications avec tous les tags vers le repo distant :
```
git push --tags
```
- Poussez juste un tag vers le repo distant :
```
git push origin <tag>
```

#### Notes spéciales sur les mauvais Tag.

Il y a plusieurs choses qui peuvent être erronées avec le marquage:
1. On peut omettre l'option -a prévue ;
2. On peut écrire une mauvaise version du tag; ou
3. On peut omettre ou (horreur!) réparer un fichier et souhaiter mettre à jour le tag.

*Si vous n'avez pas encore poussé le tag* le correctif est trivial: pour les deux premiers cas, supprimez le tag erroné git tag -d <tag>; pour le troisième, re-tag avec ```git tag -am <message> [<commit>]```. 
Cependant , si vous avez déjà poussé le tag, il y a des conséquences plus larges.
Pour cette raison, la modification d'un tag poussé est fortement découragé: créez un nouveau tag. 
Cependant, puisque vous allez ignorer mon conseil et le faire de toute façon, voici comment faire ce que vous voulez:
1. ```git push -d origin <tag>```
2. Revenez dans votre répertoire de travail, marquez correctement, puis envoyez le tag vers le dépôt distant.
3. Maintenant, vous devez alerter tous vos développeurs que, s'ils ont rapatrié un tag erronée dans leur dépôt local, ils devront enlever ce tag de leur dépôt local ```git -d <tag>``` sinon, les étiquettes supprimées continueront à apparaître dans le référentiel distant et/ou les utilisateurs ne pourront plus pousser les modifications sur le dépôt distant.

### Annuler le dernier commit:

- Annuler le commit:
```
git reset --soft HEAD^
```
- Faire des choses.
- Recommit:
```
git commit -a -m <message> -c ORIG_HEAD
```
Notez que l'option -c ORIG_HEAD fait en sorte que git utilise les méta-données du HEAD précédent (auteur, etc.) à l'exception du message de commit.

### Récupérer un fichier supprimé (fichiers commités)

- Obtenez une liste de tous les commit avec les fichiers supprimés:
```
git log --diff-filter=D --summary | less
```
- Trouvez votre fichier et notez le hash SHA1 pour ce commit.
- Récupérez-le:
```
git checkout <commit>^ -- file
```

### Sélection des blocs de texte à commiter dans un fichier

```
git add --patch
```
Suivez les instructions à l'écran.

### Picorer des commits dans différentes branches et les appliquer dans ma branche

La commande ```git cherry-pick``` permet de picorer des commits à droite, à gauche et venir les appliquer dans notre branche.

### Plusieurs copies de travail sur un seul dépôt.

La commande ```git worktree``` permet de travailler en parallèle sur plusieurs copies de travail dans un seul dépôt.

### Comment retrouver un commmit que l'on pense avoir définitivement supprimer ?

La commande ```git reflog``` affiche l'ensemble des commits que nous avons parcouru. Il est donc possible de retrouver un commit qui n'est plus référencé.

### Comment retrouver rapidement un bug dans du code.

La commande ```git bisect``` effectue une recherche dicotomique par rapport au code d'erreur d'un script lancé dans chaque commit.

### Qui a modifier cette ligne de code ?

La commande ```git blame``` va m'aider à retouver le coupable.


